extern crate proc_macro;
use proc_macro::{TokenStream};
use std::fmt::Debug;
use std::str::FromStr;

/// Produce a new array from existing array(s).
/// The given arrays will be concatenated together, iterating over the indicies `start..end`
/// for each of them. You can optionally specify a `capacity` and `default` value to fill for
/// in for padding.
///
/// # Usage
///
/// `new_array!(arr, start, end, [arr, start, end]..., [capacity, default])`
///
/// # Arguments
///
/// - `arr`   : an existing array
/// - `start` : the index in the array to begin taking elements from
/// - `end`   : the index in the array to stop taking elements
/// - `capacity` : the total size of the array. this should be used when the size of your output array
///                is greater than the size of the sum of the length of the provided arrays. the paramater `default`
///                is required.
/// - `default` : the default value to insert when moving to a larger sized array (e.g 0 for integers)
///
/// # Panics
///
/// The macro will fail if the capacity is less than the total sum of the provided arrays, or you supply invalid arguments.
/// 
/// # Issues
///
/// The argument list obtained by turning the token stream into a string and splitting by a comma.
/// Because of this, you will run into issues when an array parameter contains a comma.
/// for example `new_array!(foo[","]...)`
/// 
/// To get around this bind your array to a variable
/// `let arr = foo[","]`
/// 
/// # Examples
///
/// The following example will create an array that holds 100 elements, having a default value of 0
/// The beginning of the array will contain the concatenation of `arr` and `arr2` with the remainder holding
/// zero values.
///
/// If you wanted only to concat two arrays you would use `let both = new_array!(arr, 0, 5, arr2, 0, 10);`
/// which would return an array of size 15
///
/// ```
/// #![feature(proc_macro_hygiene)]
/// use array_tools::new_array;
///
/// fn main() {
///     let arr = [3;10];
///     let arr2 = [2;10];
///
///     // concatenate two arrays with a capacity of 100 and a default of 0
///     let smaller = new_array!(arr, 0, 5, arr2, 0, 10, 100, 0);
///
///     println!("array length: {:?}", smaller.len());
/// }
/// ```
#[proc_macro]
pub fn new_array(item: TokenStream) -> TokenStream {
    // filter out punctuation tokens then collect passed arguments into
    // a vector of strings
    let arguments = parse_arguments(item);

    // Output vector containing the list of all array elements.
    // At the end this will be joined into a comma separated string
    // and returned in as an array literal
    let mut output = Vec::new();

    // the total length of all the arrays passed into the macro
    // this is subtracted from capacity, if it exists, to obtain the
    // number of default values to insert for padding
    let total_length = build_array_groups(&arguments, &mut output);

    // check if a capacity was specified. this is true if there was a group of less than
    // three arguments passed. the capacity group is two elements so the remainder will be two
    // if it was set.
    if arguments.len() % 3 == 2 {
        let a = arguments.len();
        let default: &str = &arguments[a - 1];
        let capacity: u64 = must_parse(&arguments[a - 2]);

        // panic!("total length is {}, capacity is {}", total_length, capacity);
        if capacity < total_length {
            panic!("capacity is less than the total length of the input arrays");
        }

        // pad with default values
        for _ in 0..capacity - total_length {
            output.push(default.to_string());
        }
    }

    let out = output.join(", ");
    let out = format!("[{}]", out);

    // panic!("{}", out);
    out.parse().unwrap()
}

fn must_parse<F>(input: impl AsRef<str>) -> F
where
    F: FromStr,
    F::Err: Debug,
{
    input.as_ref().parse().unwrap()
}

fn parse_arguments(input: TokenStream) -> Vec<String> {
    input
        .to_string()
        .split(",")
        .map(|x| x.trim())
        .map(String::from)
        .collect()
}

fn build_array_groups<T: AsRef<str>>(arguments: &[T], output: &mut Vec<String>) -> u64 {
    let mut total_length = 0;

    // length of each group of arguments
    let group_len = 3;
    let groups = arguments.len() / group_len;
    
    for i in 0..groups {
        let idx = i * group_len;
        let array: &str = arguments[idx + 0].as_ref();
        let start: u64 = must_parse(&arguments[idx + 1].as_ref());
        let end: u64 = must_parse(&arguments[idx + 2].as_ref());

        for i in start..end {
            output.push(format!("{}[{}]", array, i));
        }

        total_length += end - start;
    }

    total_length
}



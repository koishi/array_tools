 # array_tools
 This crate exposes the macro `new_array` for concatenating and resizing fixed size arrays.

  ```toml
 array_tools = { git = "https://gitlab.com/koishi/array_tools.git" }
 ```

Produce a new array from existing array(s).
The given arrays will be concatenated together, iterating over the indicies `start..end`
for each of them. You can optionally specify a `capacity` and `default` value to fill for
in for padding.

# Usage

`new_array!(arr, start, end, [arr, start, end]..., [capacity, default])`

# Arguments

- `arr`   : an existing array
- `start` : the index in the array to begin taking elements from
- `end`   : the index in the array to stop taking elements
- `capacity` : the total size of the array. this should be used when the size of your output array
               is greater than the size of the sum of the length of the provided arrays. the paramater `default`
               is required.
- `default` : the default value to insert when moving to a larger sized array (e.g 0 for integers)

# Panics

The macro will fail if the capacity is less than the total sum of the provided arrays, or you supply invalid arguments.

# Issues

The argument list obtained by turning the token stream into a string and splitting by a comma.
Because of this, you will run into issues when an array parameter contains a comma.
for example `new_array!(foo[","]...)`

To get around this bind your array to a variable
`let arr = foo[","]`

# Examples

The following example will create an array that holds 100 elements, having a default value of 0
The beginning of the array will contain the concatenation of `arr` and `arr2` with the remainder holding
zero values.

If you wanted only to concat two arrays you would use `let both = new_array!(arr, 0, 5, arr2, 0, 10);`
which would return an array of size 15

```
#![feature(proc_macro_hygiene)]
use array_tools::new_array;

fn main() {
    let arr = [3;10];
    let arr2 = [2;10];

    // concatenate two arrays with a capacity of 100 and a default of 0
    let smaller = new_array!(arr, 0, 5, arr2, 0, 10, 100, 0);

    println!("array length: {:?}", smaller.len());
}
```

take a look at the tests crate for more usage examples
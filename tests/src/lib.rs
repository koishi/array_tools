#![feature(proc_macro_hygiene)]

#[cfg(test)]
mod tests {
    use array_tools::new_array;

    struct Foo {
        data: [u64; 10],
    }

    #[test]
    fn test_new_array() {
        let arr = [0; 10];
        let arr2 = [1; 20];

        let foo = Foo { data: [4; 10] };

        assert_eq!(new_array!(arr, 0, 3), [0, 0, 0]);
        assert_eq!(new_array!(arr, 0, 3, 4, 1), [0, 0, 0, 1]);
        assert_eq!(new_array!(arr, 0, 3, arr2, 0, 3), [0, 0, 0, 1, 1, 1]);
        assert_eq!(
            new_array!(arr, 0, 3, arr2, 0, 3, 9, 3),
            [0, 0, 0, 1, 1, 1, 3, 3, 3]
        );
        assert_eq!(new_array!(foo.data, 0, 3), [4, 4, 4]);
    }
}
